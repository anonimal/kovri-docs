## Garanzia di qualità
- Leggi [Quality Assurance](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/it/quality.md) per farti un'idea riguardo le nostre attività.

## Conformità
- Puntiamo alla completa conformità con C++11/14; utilizzalo liberamente per svolgere il tuo lavoro.
- È altamente consigliato l'utilizzo delle librerie standard e delle relative dipendenze quando possibile.

## Modalità di invio del tuo lavoro
Per inviare i tuoi contributi, ti chiediamo di procedere con la seguente modalità:

1. Fork Kovri
2. Leggi la nostra [style guide](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/it/style.md)
3. Crea un [topic branch](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
4. [**Firma**](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) i tuoi commit.
5. Invia una pull-request al branch ```master```
   - Poichè siamo in una fase pre-alfa non abbiamo tag. Per il momento, puoi sviluppare il tuo lavoro al di fuori del master.
   - I commit devono essere verbose di default, costituiti da una breve spiegazione (50 caratteri al massimo), una riga vuota, e una spiegazione dettagliata in paragrafi separati, a meno che il titolo stesso non sia già chiaro di per sé.
   - Il titolo del commit dovrebbe iniziare con la categoria o l'aspetto del progetto. Ad esempio, "HTTPProxy: implement User-Agent scrubber. Fixes #193." oppure "Garlic: fix uninitialized padding in ElGamalBlock".
   - Aggiungi un riferimento nel caso in cui un commit faccia riferimento ad un altro problema. Ad esempio "See #123" o "Fixes #123". Questo ci aiuterà a risolvere i ticket quando arriveranno in ```master```.
   - In generale, i commits devono essere [atomic](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention) e i diffs devono essere di facile lettura. Per questo motivo, ti chiediamo di non mischiare fix formattati con commit non formattati.
   - Il corpo della richiesta pull dovrebbe contenere una descrizione dettagliata del funzionamento della specifica patch e della motivazione/giustificazione per la sua introduzione (se necessaria). Dovresti includere i riferimenti relativi a qualsiasi discussione come altri ticket o chat in IRC.

## Proposte
Prima di inviare una proposta fai riferimento a [issue aperte](https://gitlab.com/kovri-project/kovri/issues) per controllare se sia già presente. Nel caso in cui non ci sia procedi pure ad [aprirne una nuova](https://gitlab.com/kovri-project/kovri/issues/new).

Ti chiediamo di iviare una proposta per i motivi seguenti:

1. Una proposta apre alla comunicazione.
2. Una proposta mostra rispetto per il lavoro svolto dagli altri contributori.
3. Una proposta permette di avere soluzione di continuità delle differenti modifiche in maniera accessibile.
4. Una proposta fa risparmiare tempo ad un altro collaboratore che si sta occupando della medesima feature o del medesimo fix. 
5. Una proposta evita catastrofi e contrattempi oppure prepara tutti i collaboratori a catastrofi e contrattempi.
*Non* aprire una proposta *non* ti impedirà di contribuire; procederemo comunque al merge di ciò che hai prodotto - ma, in ogni caso, una proposta è altamente consigliata.

## TODO's
- Fai una veloce ricerca nel codebase per ```TODO(unassigned):```, scegli un ticket e inizia a patchare!
- Se crei un TODO, comincia a lavorarci tu oppure scrivi ```TODO(unassigned):```
