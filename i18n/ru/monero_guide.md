# Monero через Kovri

## Начало

1. Следуйте инструкциям по сборке и/или установке [Kovri](https://gitlab.com/kovri-project/kovri) & [Monero](https://github.com/monero-project/monero)
2. Прочтите [Руководство пользователя](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/user_guide.md) на вашем языке.
3. Настройте серверные и клиентские туннели Kovri для подключения к узлам `monerod` через сеть I2P

## Туннель сервера Kovri

Сначала настройте туннель сервера на одном узле.

Чтобы настроить туннель сервера, смотрите `tunnels.conf`, который расположенный в каталоге data. Чтобы найти каталог data, загляните в `Kovri.conf`.

```
[XMR_P2P_Server]
type = server
address = 127.0.0.1
port = 28080
in_port = 28080
keys = xmr-p2p-keys.dat
;white_list =
;black_list =
```

This configuration will open a Kovri listener on `monerod`'s default testnet P2P port. Next, read the [User Guide](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/user_guide.md) for how to obtain the base32 destination address.

Если вы уже запустили свой маршрутизатор kovri, запустите `$ kill -HUP $ (pgrep kovri)` для загрузки нового туннеля. Вы также можете выполнить жесткий перезапуск, закрыв и запустив kovri повторно.

## Клиентский туннель Kovri

Теперь, когда туннель сервера настроен, запустите настройку туннеля клиента, указав на туннель сервера.

Пример:

```
[XMR_Client]
type = client
address = 127.0.0.1
port = 24040
dest = your-copied-server-destination.b32.i2p
dest_port = 28080
keys = xmr-client-keys.dat
```

Повторите процесс для каждого узла, который вы хотите подключить, используя Kovri.

Если вы уже запустили свой маршрутизатор kovri, запустите `$ kill -HUP $ (pgrep kovri)` для загрузки нового туннеля. Вы также можете выполнить жесткий перезапуск, закрыв и запустив kovri повторно.

## Monero P2P через Kovri

Указать `monerod` на ваш новый клиентский туннель Kovri очень просто.

Пример запуска узла в тестовой сети:

```bash
monerod --testnet --start-mining your-testnet-wallet-address --add-exclusive-node 127.0.0.1:24040
```

Если вы обнаружили проблемы с подключением, подождите, пока ваши узлы Kovri будут интегрированы в сеть (ориентировочно, через 5-10 минут после запуска).

## Monero RPC через Kovri

Проброс службы RPC Monero через Kovri - это аналогичный процесс.

Настройте туннель сервера на узле Kovri:

```
[XMR_RPC_Server]
type = server
address = 127.0.0.1
port = 28081
in_port = 28081
keys = xmr-rpc-keys.dat
;white_list =
;black_list =
```

Это создаст новый набор ключей и новый адрес назначения.

Чтобы использовать тот же адрес назначения, что и P2P, просто замените `xmr-rpc-keys.dat` на `xmr-p2p-keys.dat` в примере настройки конфигурации.

Если вы уже запустили свой маршрутизатор kovri, запустите `$ kill -HUP $ (pgrep kovri)` для загрузки нового туннеля. Вы также можете выполнить жесткий перезапуск, закрыв и запустив kovri повторно.

В этом туннеле установлен тестовый JSON RPC по умолчанию для `monerod`, и для подключения можно использовать туннель HTTP клиента Kovri.

Ниже приведен пример подключения на curl из клиентского узла Kovri:

```bash
export http_proxy=127.0.0.1:4446 rpc_server=http://your-copied-rpc-destination.b32.i2p:28081
curl -X POST ${rpc_server}/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_height"}' -H 'Content-Type: application/json'
```
