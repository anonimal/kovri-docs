# Dus je hebt Kovri geïnstalleerd. Wat nu?

## Stap 1. Open je NAT/firewall.
1. Kies een port tussen ```9111``` en ```30777```.
2. **Sla deze poort op in je configuratiebestand** (`kovri.conf`).
3. Geef in de NAT/firewall toestemming voor die poort aan inkomende TCP/UDP-verbindingen. (Zie onderstaande opmerkingen als je geen toegang hebt).

Opmerkingen:

- Als je de poort niet opslaat, wordt bij het opstarten steeds een willekeurige nieuwe poort gekozen door kovri. (Maar je kunt ook de poort bij het opstarten opgeven met de parameter `--port`.)
- Als je geen toegang hebt tot je NAT, gebruik je de runtime-optie `--enable-upnp` of schakel je deze optie in `kovri.conf` in.
- **Vertel niemand welk poortnummer je gebruikt, want dat zou je minder anoniem maken.**

## Stap 2. (Aanbevolen) Beveiligingsadvies

- Maak eventueel een speciale gebruiker met de naam `kovri` aan en laat alleen die gebruiker kovri uitvoeren.
- Als je Linux gebruikt, kun je een geharde kernel gebruiken (bijvoorbeeld [grsec](https://en.wikibooks.org/wiki/Grsecurity) met RBAC).
- Nadat je de benodigde resources hebt geïnstalleerd in het gevenspad voor kovri, kun je er toegangsbeheer voor instellen met [setfacl](https://linux.die.net/man/1/setfacl), [umask](https://en.wikipedia.org/wiki/Umask) of de toegangsbeheerfunctie van het besturingssysteem.
- Vertel niemand welk poortnummer je gebruikt, want dat zou je minder anoniem maken.

**Opmerking: zie kovri.conf voor het gegevenspad in Linux/macOS/Windows**

## Stap 3. Configureer Kovri

Een volledige lijst met opties vind je via:

```bash
# Linux/macOS/ *BSD
$ cd ~/bin && ./kovri --help
```

```bash
# Windows (PowerShell/MSYS2)
$ cd "C:\Program Files\Kovri" ; ./kovri.exe --help
```

Alle opties met details vind je in:

- `kovri.conf`, configuratiebestand voor router en client
- `tunnels.conf`, configuratiebestand voor client- en servertunnels

## Stap 4. (Optioneel) Stel tunnels in

*Client-tunnels* zijn tunnels waardoor je verbinding maakt met andere services. *Servertunnels* gebruik je om services te hosten zodat andere mensen verbinding kunnen maken met je service (website, SSH etc.)

Standaard zijn er client-tunnels ingesteld voor IRC (Irc2P) en e-mail (i2pmail). Zie `tunnels.conf` voor het toevoegen/verwijderen van tunnels.

Wanneer je servertunnels aanmaakt, moet je *persistente privésleutels* genereren die worden gebruikt voor je bestemming. Haal daarvoor de commentaartekens voor `keys = your-keys.dat` weg, of voeg die regel toe, en vervang `your-keys` door een geschikte naam. **Deel je persoonlijke `.dat`-bestand met niemand (tenzij je multi-homing wilt toepassen) en zorg dat je een back-up van het bestand maakt.**

Als dit is ingesteld, wordt je bestemming in je log weergegeven na het starten van kovri, gecodeerd in de vorm van een [Base32-adres](https://getmonero.org/resources/moneropedia/base32-address) en een [Base64-adres](https://getmonero.org/resources/moneropedia/base64-address). Je vindt het gecodeerde adres ook in een tekstbestand, in dezelfde directory `client/keys` in het gegevenspad voor kovri als het `.dat`-bestand. De gecodeerde bestemming in de **tekstbestanden** `.dat.b32.txt` en `.dat.b64.txt` zijn veilig om aan anderen te geven, zodat ze verbinding kunnen maken met uw service.

Voorbeeld:

- Bestand met privésleutels: `client/keys/mijnsleutels.dat`
- Openbaar [Base32-adres](https://getmonero.org/resources/moneropedia/base32-address): `client/keys/mijnsleutels.dat.b32.txt`
- Openbaar [Base64-adres](https://getmonero.org/resources/moneropedia/base64-address): `client/keys/mijnsleutels.dat.b64.txt`

**Opmerking: zie kovri.conf voor het gegevenspad in Linux/macOS/Windows**

## Stap 5. (Optioneel) Registreer je nieuwe [eepsite](https://getmonero.org/resources/moneropedia/eepsite).

**Stop! Registreer je service liever alleen bij Kovri en *niet* bij stats.i2p totdat issue [#498](https://gitlab.com/kovri-project/kovri/issues/498) is opgelost.**

- Open een verzoek met de tekst `[Subscription Request] mijnhost.i2p` (vervang `mijnhost.i2p` door de gewenste hostname) in de [Kovri-issuetracker](https://gitlab.com/kovri-project/kovri/issues).
- Plak in de hoofdtekst van het verzoek de inhoud van het in de vorige stap genoemde openbare `.txt`-bestand.
- Nadat je host is goedgekeurd, wordt je host toegevoegd en wordt het abonnement ondertekend.
- Klaar is Kees!

## Stap 6. Voer Kovri uit
```bash
$ cd build/ && ./kovri
```
- Wacht ongeveer 5 minuten totdat je host wordt herkend door het netwerk voordat je probeert services te gebruiken.

## Stap 7. Chat met ons via IRC.
1. Start een [IRC client](https://nl.wikipedia.org/wiki/Internet_Relay_Chat#Clients).
2. Stel je client in om verbinding te maken met de IRC-poort voor kovri (standaard 6669). Zo maak je verbinding met het Irc2P-netwerk (het IRC-netwerk van I2P).
3. Open de kanalen `#kovri` en `#kovri-dev`.

## Stap 8. Bezoek een I2P-website (garlic-site/eepsite)
1. Start een browser naar keuze (liefst een browser die je alleen voor kovri gebruikt).
2. Configureer de browser door [deze instructies](https://geti2p.net/en/about/browser-config) te lezen, maar **in plaats van poort 4444 en 4445** verander je de HTTP-proxypoort in **4446** en verander je de SSL-proxypoort *ook* in **4446**.
3. Ga naar http://check.kovri.i2p

Opmerkingen:

- ** Net als met Tor heb je geen SSL nodig om het netwerk veilig te gebruiken.**
- Er worden momenteel geen SSL-sites of outproxy-services ondersteund.
- Als iemand je een .i2p-adres geeft dat niet in je adresboek staat, gebruik je de `Jump`-service op http://stats.i2p/i2p/lookup.html.
- Raadpleeg het bestand hosts.txt in je gegevensdirectory voor een lijst met standaardsites die je kunt bezoeken.
- De implementatie van de HTTP-proxy en het adresboek is nog in ontwikkeling en er ontbreken nog functies.

## Stap 9. Veel plezier!
- Lees meer over Kovri in de [Moneropedia](https://getmonero.org/resources/moneropedia/kovri).
- Open functieverzoeken of meld bugs via onze [issuetracker](https://gitlab.com/kovri-project/kovri/issues).
- Lees meer over het I2P-netwerk op de [Java I2P-website](https://geti2p.net/en/docs).

# Containeropties

## Snapcraft

Op Linux-systemen kun je snapcraft gebruiken om Kovri eenvoudig te installeren.

### Stap 1. Kopieer de repository met de Kovri-broncode

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
```

### Stap 2. Installeer snapcraft

- Gebruik de packagemanager van je distributie voor snapcraft en [snapd](https://snapcraft.io/docs/core/install).

In Ubuntu voer je simpelweg het volgende uit:
```bash
$ sudo apt-get install snapcraft
```

### Stap 3. Maak de snap

```bash
$ cd kovri/ && snapcraft && sudo snap install *.snap --dangerous
```
Opmerking: de parameter --dangerous is alleen nodig omdat de snap niet is ondertekend. Maar dat is geen probleem, want je hebt de snap immers zelf gemaakt.

### Stap 4. Voer Kovri uit met snapcraft

```bash
$ snap run kovri
```

## Docker

### Stap 1. Installeer Docker
Het installeren van Docker valt buiten het bereik van dit document. Zie daarvoor de [Docker-documentatie](https://docs.docker.com/engine/installation/).

### Stap 2. Configureren/firewall openstellen

De Docker-image wordt geleverd met de standaardinstellingen van kovri, maar kan worden geconfigureerd, zoals hierboven uitgelegd.

Kies een willekeurige poort en zet die poort open (zie hierboven).

### Stap 3. Actief

#### Standaardinstellingen
```bash
KOVRI_PORT=42085 && sudo docker run -p 127.0.0.1:4446:4446 -p 127.0.0.1:6669:6669 -p $KOVRI_PORT --env KOVRI_PORT=$KOVRI_PORT geti2p/kovri
```

#### Aangepaste instellingen
Hierbij bevat `./kovri-settings/` de configuratiebestanden `kovri.conf` en `tunnels.conf`.
```bash
KOVRI_PORT=42085 && sudo docker run -p 127.0.0.1:4446:4446 -p 127.0.0.1:6669:6669 -p $KOVRI_PORT --env KOVRI_PORT=$KOVRI_PORT -v kovri-settings:/home/kovri/.kovri/config:ro geti2p/kovri
```
